package com.tv2.transformer;
import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.xpack.client.PreBuiltXPackTransportClient;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class Writer {
	
	private ArrayList<JSONObject> docToWrite;
	private JSONObject jsonDoc;
	private JSONArray arrayIngest;
	private ArrayList<JSONObject> list;
	
	
	@SuppressWarnings("unchecked")
	public void writer(HashMap<String,ArrayList<JSONObject>> ingest, HashMap<String,JSONObject> docs) throws IOException{
			
		list = new ArrayList<JSONObject>();
		for (Map.Entry<String, ArrayList<JSONObject>> entry : ingest.entrySet()) {
			docToWrite = entry.getValue();					
			arrayIngest = new JSONArray();
			
			for (JSONObject jsonObject : docToWrite) {							
				arrayIngest.add(jsonObject);						
			}
			
			jsonDoc = docs.get(entry.getKey());			
			jsonDoc.put("ingestContent", arrayIngest);						
			list.add(jsonDoc);
			System.out.println("\n"+jsonDoc.toJSONString()+"\n");
		
		}
		
		writeToIndex("");						
	}
	
	public void writeToIndex(String elasticIndex) throws IOException{
		TransportClient client;
		//Settings settings = Settings.builder().put("xpack.security.user", "kibana:conviva").build();   			
		Settings settings = Settings.builder().build();   	
		client = new PreBuiltXPackTransportClient(settings);   	
		client.addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("localhost"),9300));  	
		
		
		@SuppressWarnings("unused")
		IndexResponse response = client.prepareIndex("demo-index", "test", "1")
		        .setSource(jsonBuilder()
		                    .startObject()
		                        .field("user", "Trond")
		                        .field("message", "trying out Elasticsearch API")
		                    .endObject()).get();
		
								client.close();
		      
	}
	
	

	@SuppressWarnings("unchecked")
	public void writerFile(HashMap<String,ArrayList<JSONObject>> ingest, HashMap<String,JSONObject> docs) throws IOException{
		
		list = new ArrayList<JSONObject>();
		for (Map.Entry<String, ArrayList<JSONObject>> entry : ingest.entrySet()) {
			docToWrite = entry.getValue();					
			arrayIngest = new JSONArray();
			
			for (JSONObject jsonObject : docToWrite) {							
				arrayIngest.add(jsonObject);						
			}
			
			jsonDoc = docs.get(entry.getKey());			
			JSONObject test = (JSONObject)jsonDoc.get("_source");		
			test.put("ingestContent", arrayIngest);						
			list.add(jsonDoc);
			System.out.println("\n"+jsonDoc.toJSONString()+"\n");
		
		}
				
//		jsonDoc.put("ingestContent", arrayIngest);				
//		list.add(jsonDoc);
//		System.out.println("\n"+jsonDoc.toJSONString()+"\n")
		
		try (FileWriter file = new FileWriter("data/result.json")) {		
			for (JSONObject object : list) {
				file.write(object.toJSONString()+"\n");
			}			
		}
								
	}
	
	
	

}
