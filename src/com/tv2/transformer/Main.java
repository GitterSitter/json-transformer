package com.tv2.transformer;

import java.io.IOException;
import java.net.UnknownHostException;

import com.tv2.transformer.*;
	 
	
public class Main {


	public static void main(String[] args) throws IOException, NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
				
		
			
		Reader reader = new Reader();
		long startTime = System.currentTimeMillis();
		reader.readIndexFromEsApi();
		long endTime  = System.currentTimeMillis();
		long totalTime = (endTime - startTime) / 1000;
		System.out.println("\n\n" + totalTime + " seconds");


	}
		
		

}
