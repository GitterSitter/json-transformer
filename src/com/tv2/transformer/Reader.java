package com.tv2.transformer;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.elasticsearch.search.SearchHit;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.tv2.transformer.*;


public class Reader {
	
	public JSONParser parser;
	public HashMap<String, ArrayList<JSONObject>> ingestMapping;
	public HashMap<String, JSONObject> docMapping;
	public ArrayList<JSONObject> jsonArray;
	public ArrayList<JSONObject> ingestDocs;
	private BufferedReader bufferedReader;
    private FileReader fileReader;
	private Writer writer;
	
	
	public void readIndexFromEsApi() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException{

		ingestDocs = new ArrayList<JSONObject>();
		ingestMapping = new HashMap<String, ArrayList<JSONObject>>();
		docMapping = new HashMap<String, JSONObject>();  
		
		JSONObject obj;	
		SearchHit[] metadataHits = null;
		SearchHit[] ingestHits = null;
		String id = "";
		
		EsApi es = new EsApi();		
		    try {
		    	metadataHits = es.init("metadataexport","program");
		    	ingestHits = es.init("dev-met-workflow-1","workflow");
			} catch (Exception e) {
				e.printStackTrace();
			}


		    try {
		    	for (SearchHit hit : ingestHits) {	       	
		    		obj = (JSONObject) new JSONParser().parse(hit.sourceAsString());
		    		ingestDocs.add(obj);  		
		    	}
		    	for (SearchHit searchHit : metadataHits) {	    			    
		    		obj = (JSONObject) new JSONParser().parse(searchHit.sourceAsString());			
		    		id = (String) obj.get("aggregateId"); 
		   
		    		jsonArray = new ArrayList<JSONObject>();
		    		for (JSONObject doc : ingestDocs) {					    		
		    			String programID = (String) doc.get("programId");
		    			if(id.equals(programID)){
		    				jsonArray.add(doc);
		    				System.out.println(programID + " found!");		            		
		    			}		
		    		}

		    		if(!jsonArray.isEmpty()){
		    			ingestMapping.put(id, jsonArray);
		    			docMapping.put(id,obj);   
		    		}else{
		    			jsonArray = null;
		    		}
		    	}

		    	System.out.println("ingestDocs found: "+ ingestDocs.size() );
		    	System.out.println("matches found: "+ ingestMapping.size());
		    	System.out.println("metadata found: "+ metadataHits.length);

		    	writer = new Writer();
		    	writer.writer(ingestMapping,docMapping);
		    	


		    }
		    catch(Exception ex) {	

		    }
			
 } 		  
	
	
		//Unused
	public void readJsonArray(){
		try {
			parser = new JSONParser();
			fileReader = new FileReader("data/test.json");

			JSONArray ar = (JSONArray) parser.parse(fileReader);
			Iterator<?> i = ar.iterator();

			int count = 0;
			while (i.hasNext()) {
				JSONObject obj = (JSONObject) i.next();
				String a = (String) obj.get("_index");
				String b = (String) obj.get("_id");   
				count++;
				System.out.println(a + " " + b + " " + count);                              
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
/*
	{ "id":"da7504fa-0b47-4bdb-b1e3-27a7c3222d1d","type": "INGEST_VIDEO_RESOURCE","status": "CREATED","programId": "534415e3-7b6d-4433-a610-3fabdac2dd21", "jobs": []}
	{ "id":"d96ec599-70c3-40fd-89e5-2f875ae4fee7","type": "INGEST_VIDEO_RESOURCE","status": "CREATED","programId": "534415e3-7b6d-4433-a610-3fabdac2dd21","jobs": []}
	{ "id":"d96ec599-70c3-40fd-89e5-2f875ae4fee7","type": "INGEST_VIDEO_RESOURCE","status": "CREATED","programId": "534415e3-7b6d-4433-1111-3fabdac2dd21","jobs": []}

	*/
	

public void readIndexFromFile() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException{

		ingestDocs = new ArrayList<JSONObject>();
		ingestMapping = new HashMap<String, ArrayList<JSONObject>>();
		docMapping = new HashMap<String, JSONObject>();  
		JSONObject obj;

		String metadataFile = "data/metadataexport.json";
		String ingestDataFile = "data/ingest.json";	
		String line = null;
		String id = "";

		try {

			fileReader = new FileReader(ingestDataFile);
			bufferedReader = new BufferedReader(fileReader);
			while((line = bufferedReader.readLine()) != null) {	        	
				obj = (JSONObject) new JSONParser().parse(line);
				ingestDocs.add(obj);  	
			}

			fileReader.close();
			bufferedReader.close();  

			fileReader = new FileReader(metadataFile);
			bufferedReader = new BufferedReader(fileReader);

			JSONObject first = null;
			JSONObject second = null;
		  while((line = bufferedReader.readLine()) != null) {	    
			   	obj = (JSONObject) new JSONParser().parse(line);			
				 first = (JSONObject)obj.get("_source");	
				 second = (JSONObject)first.get("idMap");
				 id = (String) first.get("aggregateId");
				jsonArray = new ArrayList<JSONObject>();
				for (JSONObject doc : ingestDocs) {
					second = (JSONObject)doc.get("_source");					
					String programID = (String) second.get("programId");
					
					if(id.equals(programID)){
						jsonArray.add(second);
						System.out.println(programID + " found!");		            		
					}		
				}

				if(!jsonArray.isEmpty()){
					ingestMapping.put(id, jsonArray);
					docMapping.put(id,obj);   
				}else{
					jsonArray = null;
				}

			}
			bufferedReader.close();   

			System.out.println("ingestDocs found: "+ ingestDocs.size() );
			System.out.println("mapping indexes: "+ingestMapping.size());
			System.out.println("document corpus size: "+docMapping.size());
		      
		}
		catch(FileNotFoundException ex) {
			System.out.println("Unable to open file '" + metadataFile + "'");                
		}
		catch(IOException ex) {
			System.out.println("Error reading file '" + metadataFile + "'");                  
		} catch (ParseException e) {		       
			e.printStackTrace();
		}	

			writer = new Writer();
		try {
			writer.writer(ingestMapping,docMapping);
		} catch (IOException e) {
			e.printStackTrace();
		}			  
	}
	
	
}
