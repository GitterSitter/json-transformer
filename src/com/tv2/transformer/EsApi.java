package com.tv2.transformer;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.xpack.client.PreBuiltXPackTransportClient;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class EsApi 
{
	
	private  TransportClient client ;
	private  SearchResponse response ;
	public String output;
	
	
	public SearchHit[] init(String index, String type) throws UnknownHostException, ParseException {

		Settings settings = Settings.builder().put("xpack.security.user", "kibana:conviva").put("client.transport.ignore_cluster_name", true).build();   	
		client = new PreBuiltXPackTransportClient(settings);   	
		client.addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("elastic10.tv2.no"),9300));  	

		 SearchHit[] hits = null;
		try{
			    			
			//SearchResponse response1 = client.prepareSearch("metadataexport", "dev-met-workflow-1")
			 //       .setTypes("program", "workflow")
			      //  .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
			      //  .setQuery(QueryBuilders.termQuery("multi", "test"))                 // Query
			      //  .setPostFilter(QueryBuilders.rangeQuery("age").from(12).to(18))     // Filter
			       // .setFrom(0).setSize(10000).setExplain(true)
			      //  .get();
			
//			 hits = response.getHits().getHits();
//			 for (SearchHit searchHit : hits) {
//				System.out.println(searchHit.sourceAsString());
//			}
						           
			SearchResponse response = client.prepareSearch(index) //"metadataexport"
					.setTypes(type)
					//.setQuery(QueryBuilders.termQuery("homeland",true))
					.setFrom(0).setSize(10000).setExplain(true)
					.execute().actionGet();
	
		
		    hits = response.getHits().getHits();
		    
		    
		}catch(Exception e){
			e.printStackTrace();
		}       

		return hits;
	}
 
  
	public void search(){
		
		response = client.prepareSearch("metadataexport")//response = client.prepareSearch("index1", "index2")		
		.setTypes("program")// .setTypes("type1", "type2")
        //.setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
        .setQuery(QueryBuilders.termQuery("multi","homeland")).get(); 
        //.setQuery(QueryBuilders.termQuery("multi", "homeland"))// Query
       // .setPostFilter(QueryBuilders.rangeQuery("age").from(12).to(18))     // Filter
     //   .setFrom(0).setSize(60).setExplain(true).get();
			
	//	 MatchAll on the whole cluster with all default options
		 response = client.prepareSearch().get();
		
		 String output = response.toString();
         System.out.println("\n"+output);
         
         
//		  QueryBuilder qb = QueryBuilders.boolQuery().mustNot(QueryBuilders.termQuery("user_agent", ""));
//		    SearchResponse scrollResp = client.prepareSearch("demo_risk_data")
//		        .addSort(SortParseElement.DOC_FIELD_NAME, SortOrder.ASC)
//		        .setScroll(new TimeValue(60000))
//		        .setQuery(qb)
//		        .setSize(100).execute().actionGet();
//
}
    
  

}
